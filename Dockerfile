FROM golang:1.20-bullseye
ARG GITEA_ID=1000
ARG GITEA_GID=1000

RUN addgroup \
    --gid $GITEA_GID \
    gitea && \
  adduser \
    --gecos '' \
    --shell /bin/bash \
    --uid $GITEA_ID \
    --gid $GITEA_GID \
    gitea

# upgrade git to v2.39.1
RUN curl -SL https://github.com/git/git/archive/v2.39.1.tar.gz \
    | tar -xzv -C /go \
    && apt-get update \
    && apt-get install -y libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev \
    && make -C /go/git-2.39.1 prefix=/usr all \
    && make -C /go/git-2.39.1 prefix=/usr install \
    && rm -rf /go/git-2.39.1 \
# install git-lfs
    && curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash \
    && apt-get install -y git-lfs \
# install golangci-lint
    && go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.51.0 \
    && golangci-lint --version
